/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <Test/Token/Basis.h>
#include <Test/Token/Ownership.h>
#include <Test/Token/Multiplicity.h>

#include <Test/Binding/Otn/Safe/Basis.h>
#include <Test/Binding/Otn/Slim/Basis.h>
#include <Test/Binding/Otn/Raw/Basis.h>
#include <Test/Binding/Otn/CppLang/Basis.h>

#include <Test/Sample/Double.h>
#include <Test/Sample/Int.h>
#include <Test/Sample/String.h>
#include <Test/Sample/Concrete.h>
#include <Test/Sample/Derived.h>
#include <Test/Sample/NonDefault.h>
#include <Test/Sample/NonCopyable.h>
#include <Test/Sample/NonMovable.h>
#include <Test/Sample/NonAssignable.h>
#include <Test/Sample/NonCopyableNonMovable.h>

namespace Test
{

namespace Preset
{

namespace List
{

using Safe     = std::tuple<Basis::OtnSafe>;
using Slim     = std::tuple<Basis::OtnSlim>;
using Raw      = std::tuple<Basis::OtnRaw>;
using CppLang  = std::tuple<Basis::CppLang>;
using StdSmart = std::tuple<Basis::StdSmart>;

using SafeSlim = std::tuple<Basis::OtnSafe,
                            Basis::OtnSlim>;
using SafeRaw = std::tuple<Basis::OtnSafe,
                           Basis::OtnRaw>;
using SafeCppLang = std::tuple<Basis::OtnSafe,
                               Basis::CppLang>;
using SafeStdSmart = std::tuple<Basis::OtnSafe,
                                Basis::StdSmart>;
using SlimRaw = std::tuple<Basis::OtnSlim,
                           Basis::OtnRaw>;
using SlimCppLang = std::tuple<Basis::OtnSlim,
                               Basis::CppLang>;
using SlimStdSmart = std::tuple<Basis::OtnSlim,
                                Basis::StdSmart>;
using CppLangStdSmart = std::tuple<Basis::CppLang,
                                   Basis::StdSmart>;
using RawCppLang = std::tuple<Basis::OtnRaw,
                              Basis::CppLang>;
using RawStdSmart = std::tuple<Basis::OtnRaw,
                               Basis::StdSmart>;

using FullBasis = std::tuple<Basis::OtnSafe,
                             Basis::OtnSlim,
                             Basis::OtnRaw,
                             Basis::CppLang,
                             Basis::StdSmart>;

using Unified       = std::tuple<Ownership::Unified>;
using Unique        = std::tuple<Ownership::Unique>;
using Shared        = std::tuple<Ownership::Shared>;
using Weak          = std::tuple<Ownership::Weak>;
using FullOwnership = std::tuple<Ownership::Unified,
                                 Ownership::Unique,
                                 Ownership::Shared,
                                 Ownership::Weak>;

using Unknown          = std::tuple<Multiplicity::Unknown>;
using Optional         = std::tuple<Multiplicity::Optional>;
using Single           = std::tuple<Multiplicity::Single>;
using FullMultiplicity = std::tuple<Multiplicity::Unknown,
                                    Multiplicity::Optional,
                                    Multiplicity::Single>;

} // namespace List

namespace Set
{

struct Empty
{
    using Bases          = std::tuple<>;
    using Ownerships     = std::tuple<>;
    using Multiplicities = std::tuple<>;
};

struct Safe
{
    using Bases      = std::tuple<Basis::OtnSafe>;
    using Ownerships = std::tuple<Ownership::Unified,
                                  Ownership::Unique,
                                  Ownership::Shared,
                                  Ownership::Weak
                                 >;
    using Multiplicities = std::tuple<Multiplicity::Optional,
                                      Multiplicity::Single
                                     >;
};

struct Slim
{
    using Bases      = std::tuple<Basis::OtnSlim>;
    using Ownerships = std::tuple<Ownership::Unique
                                 >;
    using Multiplicities = std::tuple<Multiplicity::Optional,
                                      Multiplicity::Single
                                     >;
};

struct Raw
{
    using Bases      = std::tuple<Basis::OtnRaw>;
    using Ownerships = std::tuple<Ownership::Unified,
                                  Ownership::Unique,
                                  Ownership::Weak
                                 >;
    using Multiplicities = std::tuple<Multiplicity::Optional,
                                      Multiplicity::Single
                                     >;
};

struct CppLang
{
    using Bases          = std::tuple<Basis::CppLang>;
    using Ownerships     = std::tuple<Ownership::Unified>;
    using Multiplicities = std::tuple<Multiplicity::Unknown,
                                      Multiplicity::Optional,
                                      Multiplicity::Single
                                     >;
};

struct StdSmart
{
    using Bases      = std::tuple<Basis::StdSmart>;
    using Ownerships = std::tuple<Ownership::Unique,
                                  Ownership::Shared,
                                  Ownership::Weak
                                 >;
    using Multiplicities = std::tuple<Multiplicity::Optional>;
};

}// namespace Set

namespace Element
{

using Empty = std::tuple<>;

using Int    = std::tuple<int>;
using String = std::tuple<std::string>;

using Base      = std::tuple<Sample::Base<int>>;
using Derived   = std::tuple<Sample::Derived<int>>;
using Abstract  = std::tuple<Sample::Abstract<int>>;
using Concrete  = std::tuple<Sample::Concrete<int>>;
using Inherited = std::tuple<Sample::Abstract<int>,
                             Sample::Base<int>,
                             Sample::Derived<int>>;

using NonDefault            = std::tuple<Sample::NonDefault>;
using NonCopyable           = std::tuple<Sample::NonCopyable>;
using NonMovable            = std::tuple<Sample::NonMovable>;
using NonAssignable         = std::tuple<Sample::NonAssignable>;
using NonCopyableNonMovable = std::tuple<Sample::NonCopyableNonMovable>;
using Restricted            = std::tuple<Sample::NonDefault,
                                         Sample::NonCopyable,
                                         Sample::NonMovable,
                                         Sample::NonAssignable,
                                         Sample::NonCopyableNonMovable>;

using Complex = std::tuple<Sample::Base<int>,
                           Sample::Derived<int>>;

} // namespace Element

namespace List
{

using SubjectElement      = Element::Int;
using SubjectBasis        = FullBasis;
using SubjectOwnership    = FullOwnership;
using SubjectMultiplicity = FullMultiplicity;

using SourceElement      = SubjectElement;
using SourceBasis        = SubjectBasis;
using SourceOwnership    = SubjectOwnership;
using SourceMultiplicity = SubjectMultiplicity;

} // namespace List

namespace Set
{

using SubjectElement = Element::Empty;
using SubjectSpec    = std::tuple<Safe,
                                  Slim,
                                  Raw,
                                  CppLang,
                                  StdSmart,
                                  Empty
                                 >;

using SourceElement = SubjectElement;
using SourceSpec    = SubjectSpec;

} // namespace Set

} // namespace Preset

} // namespace Test
