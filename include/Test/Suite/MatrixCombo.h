/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <Test/Suite/Preset.h>
#include <Test/Suite/Binding.h>

#include <Test/Token/Combo.h>

namespace Test
{

template <SPEC_UNARY_FUNCTOR>
inline
void testMatrixUnaryCase()
{
    Combo::Unary::ApplySpecLists<Functor,
                                 Preset::List::SubjectElement,
                                 Preset::List::SubjectBasis,
                                 Preset::List::SubjectOwnership,
                                 Preset::List::SubjectMultiplicity>{} ();

    Combo::Unary::ApplySpecSets<Functor,
                                Preset::Set::SubjectElement,
                                Preset::Set::SubjectSpec>{} ();
}

template <SPEC_BINARY_FUNCTOR>
inline
void testMatrixBinaryCase()
{
    Combo::Binary::ApplySpecLists<Functor,
                                  Preset::List::SubjectElement,
                                  Preset::List::SubjectBasis,
                                  Preset::List::SubjectOwnership,
                                  Preset::List::SubjectMultiplicity,
                                  Preset::List::SourceElement,
                                  Preset::List::SourceBasis,
                                  Preset::List::SourceOwnership,
                                  Preset::List::SourceMultiplicity>{} ();

    Combo::Binary::ApplySpecSets<Functor,
                                 Preset::Set::SubjectElement,
                                 Preset::Set::SubjectSpec,
                                 Preset::Set::SourceElement,
                                 Preset::Set::SourceSpec>{} ();
}

} // namespace Test
